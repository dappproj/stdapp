//Import the StudentList smart contract
const StudentList = artifacts.require('StudentList')

//use the contract to write all test
//varuable: account => all accounts in blockchain
contract('StudentList', (account) => {
    //Make sure contract is deployed and before we retirve the studentlist obkect for testing
    beforeEach(async () => {     
        this.studentList = await StudentList.deployed()     //it will retive the instant of deployed contract
    })

    //Testing the deployed student contract
    it('deploys successfully', async() => {                //"it" one of the funciton of mocha testing function -- used to defined new test cases
        //Get the address which the student object is stored      // "=>" arrow function = short hand for writing function in js
    
        const address = await this.studentList.address     
        //Test for valid address
        isValidAddress(address)
    })

        //testing the content in the contract
    it('test adding students', async() => {
        return StudentList.deployed().then ((instance)=> {
        s = instance;
        studentCid = 1;

        return s.createStudent(studentCid, "Tshering Penjor");
        
        }).then ((transaction) => {         // then- tells to exectute the remaining fucntion
        isValidAddress(transaction.tx)
        isValidAddress(transaction.receipt.blockHash);
        return s.studentsCount()

        }).then ((count) => {
        assert.equal(count, 1)
        return s.students(1);

        }).then((student) => {
        assert.equal(student.cid, studentCid)
        })
    })
    // testing the content in contract
it ('test finding student', async() => {
    return StudentList.deployed().then (async (instance) => {
        s = instance;
        studentCid = 2;
        return s.createStudent(studentCid++, " Tshering Yangzom").then (async (tx) => {          // tx: transcation hash- adding student to list sucessfully
            return s.createStudent(studentCid++, "Dorji Tshering").then (async (tx) => {
                return s.createStudent(studentCid++, "Chimi Dema").then (async (tx) => {
                    return s.createStudent(studentCid++, "Tashi phuntsho").then(async (tx) => {
                        return s.createStudent(studentCid++, "Samten Zangmo").then (async (tx) => {
                            return s.createStudent(studentCid++, "Sonam Chophel").then (async (tx) =>{
                                return s.studentsCount().then(async (count) => {           // studentcount- return toltal number of student
                                    assert.equal(count, 7)

                                return s.findStudent(5).then (async (student) => {
                                    assert.equal(student.name, "Tashi phuntsho")
                                })    
                                })
                            })
                        })

                    })
                }) 
            })
        })
    })
})

// teshing changing content in the contract
it ('test mark gradute students', async() => {
    return StudentList.deployed().then(async (instance) => {
        s = instance;
        return s.findStudent(1).then(async (ostudent) => {
            assert.equal(ostudent.name,"Tshering Penjor")
            assert.equal(ostudent.graduated, false)
            return s.markGraduated(1).then(async (transaction) => {
                return s.findStudent(1).then(async(nstudent)=> {
                    assert.equal(nstudent.name, "Tshering Penjor")
                    assert.equal(nstudent.graduated, true)
                    return
                })
            })
        })
    })
})
})


//this function check id the address is valid
function isValidAddress(address){
    assert.notEqual(address, 0x0)       // check if values are not equal -- notEqual have 2 parameter
    assert.notEqual(address,'')
    assert.notEqual(address,null)
    assert.notEqual(address, undefined)
}