
import React, {Component} from 'react';
import Web3 from 'web3';
import './App.css';
import Form from './components/StudentList';
import { STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS } from './abi/config_studentList';
import { SUBJECT_LIST_ABI, SUBJECT_LIST_ADDRESS } from './abi/config_subjectList';
import { MARK_LIST_ABI, MARK_LIST_ADDRESS } from './abi/config_markList';
import SubForm from './components/SubjectList.js';
import MarkList from './components/MarkList';
import FindMarks from './components/FindMark';


class App extends Component{
  componentDidMount(){
    if(!window.ethereum)
      throw new Error("No crypto wallet found. please install it.");
    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData()
  }
  async loadBlockchainData(){
    const web3 = new Web3(Web3.givenProvider || "https://sepolia.infura.io/v3/d263c8f5f5cb41e4bd080c0ca65f1ac0")
    const accounts = await web3.eth.getAccounts()
    this.setState({account:accounts[0]})

    const studentList = new web3.eth.Contract(STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS)
    const subjectList = new web3.eth.Contract(SUBJECT_LIST_ABI,SUBJECT_LIST_ADDRESS)
    const markList = new web3.eth.Contract(MARK_LIST_ABI,MARK_LIST_ADDRESS)


    this.setState({studentList})
    this.setState({subjectList})
    this.setState({markList})

    
    const studentCount = await studentList.methods.studentsCount().call();
    const subjectCount = await subjectList.methods.subjectsCount().call();
    const markCount = await markList.methods.marksCount().call()


    this.setState({studentCount})
    this.setState({subjectCount})
    this.setState({markCount})

    //student
    this.setState.students = []
    for (var i = 1; i <= studentCount; i++) {
      const student = await studentList.methods.students(i).call();
      this.setState({
        students: [...this.state.students, student]
      })
      
    }
    //subject
    this.setState({subjectList});
    this.setState.subjects = []
    for (var j = 1; j<= subjectCount;j++){
      const subject = await subjectList.methods.subjects(j).call()
      this.setState({
        subjects: [...this.state.subjects, subject]
      })
    }

    // this.state.marks = []
    // for (var k = 1; k<= markCount;k++){
    //   const mark = await markList.methods.marks(k).call()
    //   this.setState({
    //     marks: [...this.state.marks, mark]
    //   })
    // }
  }
   
  constructor(props){
    super(props)
    this.state = {
      account: '',
      studentCount: 0,
      students: [],
      subjects:[],
      subjectsCount:0,
      marks:[],
      marksCount:0,
      loading:true,
      stdupdate:false,
      update:false,
      studentObj:{},
      updateCID:0    }

    this.createStudent = this.createStudent.bind(this)
    this.updateName = this.updateName.bind(this)
    this.updatesubName = this.updatesubName.bind(this)
    this.markGraduated = this.markGraduated.bind(this)
    this.createSubject = this.createSubject.bind(this)
    this.markRetired = this.markRetired.bind(this)
    // this.addMarks = this.addMarks.bind(this)
    this.createMarks=this.createMarks.bind(this)
    this.findMarks = this.findMarks.bind(this);
    
  }
  createStudent(cid,name){
    // alert("inside app.js",cid)
    this.setState({loading:true})
    this.state.studentList.methods.createStudent(cid,name)
    .send({from:this.state.account})
    .once('receipt',(receipt)=>{
      this.setState({loading:false})
      this.loadBlockchainData()
    })
  }
  createSubject(code,name){
    this.setState({loading:true})
    this.state.subjectList.methods.createSubject(code,name)
    .send({from:this.state.account})
    .once('receipt',(receipt)=>{
      this.setState({loading: false})
      this.loadBlockchainData()
    })
  }
  updateName(id,name){
    // alert(id+name)
    this.setState({update:true})
    this.state.studentList.methods.updateStudent(id,name)
    .send({from:this.state.account})
    .once('receipt',(receipt)=>{
      this.setState({loading:false})
      this.setState({update:false})
      this.loadBlockchainData()
    })
    this.setState({loading:false})
    this.setState({update:false})
    // alert(student._id)
  }
  updatesubName(id,name){
    // alert(id+name)
    this.setState({update:true})
    this.state.subjectList.methods.updateSubject(id,name)
    .send({from:this.state.account})
    .once('receipt',(receipt)=>{
      this.setState({loading:false})
      this.setState({update:false})
      this.loadBlockchainData()
    })
    this.setState({loading:false})
    this.setState({update:false})
    // alert(student._id)
  }

  markGraduated(cid){
    this.setState({loading:true})
    this.state.studentList.methods.markGraduated(cid)
    .send({from: this.state.account})
    .once('receipt', (receipt) => {
      this.setState({loading: false})
      this.loadBlockchainData()
    })
  }
  markRetired(cid){
    this.setState({loading:true})
    this.state.subjectList.methods.markRetired(cid)
    .send({from: this.state.account})
    .once('receipt', (receipt) => {
      this.setState({loading: false})
      this.loadBlockchainData()
    })
  }
  // addMarks(cid,code,grade){
  //   this.setState({loading:true})
  //   this.state.markList.methods.addMarks(cid,code,grade)
  //   .send({from:this.state.account})
  //   .once('receipt',(receipt)=>{
  //     this.setState({loading: false})
  //     this.loadBlockchainData()
  //   })
  // }
  // addMarks(cid,code,grade){
  //   this.setState({loading:true})
  //   this.state.markList.methods.addMarks(cid,code,grade)
  //   .send({from:this.state.account})
  //   .once('receipt',(receipt)=>{
  //     this.setState({loading: false})
  //     this.loadBlockchainData()
  //   })
  // }
  createMarks(studentid, subjectid, grades){
    this.setState({loading:true})
    this.state.markList.methods.addMarks(studentid, subjectid, grades)
    .send({from:this.state.account})
    .once('reciept',(reciept)=>{
      this.setState({loading:false})
      this.loadBlockchainData()
    })
  }
  findMarks(studentid, subjectid){
    this.setState({loading: true})
    return this.state.markList.methods
    .findMarks(studentid, subjectid)
    .call ({from: this.state.account})
  }
  render() {
    // const updateName=(event,student)=>{
    //   event.preventDefault()
    //   alert(student._id)

    // }
    return(
      <div className='container'>
        <h1>Students Marks Management System</h1>
        <p>Your account: {this.state.account}</p>

        

        <Form createStudent={this.createStudent} 
        updateName={this.updateName} update ={this.state.stdupdate} 
        studentObj = {this.studentObj} />
        <ul id='studentlsit' className='list-unstyled'>
          {
            this.state.students.map((student, key) => {
              return(
                <li className='list-group-item checkbox' key={key}>
                  <span className='name alert'> {student._id} {student.cid} {student.name} </span>
                  <input className='form-check-input' type='checkbox' name={student._id} defaultChecked ={student.graduated} 
                  disabled={student.graduated} ref={(input) => {
                    this.checkbox = input
                  }}
                  style={{ marginTop: '10px', paddingRight:'10px' }}
                  onClick={(event) => {
                    this.markGraduated(event.currentTarget.name) //event.curreentTarget:partiularr button (.name:name property of button)
                    }}
                  />
                  <label style={{fontWeight:"bold"}}className='form-check-label'>Graduated</label>
                  <button type="submit" className="btn" style={{ marginTop: '10px',border:'black' }} onClick={()=>{this.setState({stdupdate:true})
                  // this.state.updateCID = student.cid
                  this.studentObj= student
                  }} >Update</button>
                  
                </li>

              )
            })
          }  
        </ul>
        <p>Total student : {this.state.studentCount}</p>

              {/* ***subject*** */}

            
          <SubForm createSubject={this.createSubject} 
        updatesubName={this.updatesubName} update ={this.state.update} 
        subjectObj = {this.sObj} />

        <ul id='subjectlist' className='list-unstyled'>
        {this.state.subjects.map((subject, key) => (
          <li className='list-group-item' key={key}>
            <span className='name alert'>{subject._id}{subject.code}{subject.name}</span>
            <input className='form-check-input' type='checkbox' name={subject._id} defaultChecked ={subject.retired} 
                  disabled={subject.retired} ref={(input) => {
                    this.checkbox = input
                  }}
                  onClick={(event) => {
                    this.markRetired(event.currentTarget.name) //event.curreentTarget:partiularr button (.name:name property of button)
                    }}
                  />
                      <label className='form-check-label'>Retired</label>
                      <button type="submit" className="btn" style={{ marginTop: '10px',border:'red' }} onClick={()=>{this.setState({update:true})
                  this.sObj= subject
                  // console.log(subject)
                  }} >Update</button>
          </li>
        ))}
      </ul>  
                    {/* ****marks***** */}
        {/* <MarkList addMarks={this.addMarks} 
         />
      <p>Total marks : {this.state.markCount}</p>
      

      <ul id='marklist' className='list-unstyled'>
          {
            this.state.marks.map((mark, key) => {
              return(
                <li className='list-group-item checkbox' key={key}>
                  <span className='name alert'> {mark.cid} {mark.code} {mark.name} </span>
                  {/* <input className='form-check-input' type='checkbox' name={mark._id} defaultChecked ={mark.graduated} 
                  disabled={mark.graduated} ref={(input) => {
                    this.checkbox = input
                  }}
                  onClick={(event) => {
                    this.markGraduated(event.currentTarget.name) //event.curreentTarget:partiularr button (.name:name property of button)
                    }}
                  /> */}
                  {/* <label style={{fontWeight:"bold"}}className='form-check-label'>Graduated</label> */}
                  {/* <button type="submit" className="btn" style={{ marginTop: '10px',border:'red' }} onClick={()=>{this.setState({update:true})
                  // this.state.updateCID = student.cid
                  this.markObj= mark
                  }} >Update</button>
                  
                </li>

              )
            })
          }  
        </ul> */} 
        <MarkList
        subjects= {this.state.subjects}
        students= {this.state.students}
        createMarks= {this.createMarks}/>
        

        {/* <ul id='marklsit' className='list-unstyled'>
          {
            this.state.marks.map((mark, key) => {
              return(
                <li className='list-group-item checkbox' key={key}>
                  <span className='name alert'> {mark.cid} {mark.code} {mark.name} </span>
                 
                  <button className="btn btn-primary btn-sm rounded border border-lg">Update</button>
                  <button className="btn btn-success btn-sm rounded border border-lg" onClick={()=>{this.setState({update:true})
                  this.markObj= mark
                  }}>
                    update
                  </button>
                </li>

              )
            })
          }  
        </ul> */}
        <p>Total Marks : {this.state.markCount}</p>
        <FindMarks subjects={this.state.subjects} 
                     students={this.state.students} 
                     findMarks={this.findMarks}/>
      </div>
    )
  }
}


export default App;
