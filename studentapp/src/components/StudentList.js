import React, { useState } from 'react';

function Form(props) {
  const [name, setName] = useState("")
  const [formData, setFormData] = useState({
    cid: '',
    name: '',
  });

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    const {cid,name} = formData
    if(cid==="" || name ===""){
      alert("cid and name cannot be empty")
    }
    props.createStudent(cid,name)
    setFormData({
      cid: '',
      name: '',
    })
    // alert("hello")
  };
  const handleUpdateName = (e) => {
    e.preventDefault();
    props.updateName(props.studentObj._id, name);
    // setFormData({
    //   cid: '',
    //   name: '',
    // });
  };
  

  return (
    <form  className="p-5 border justify-content-center w-100">
      <h1>Add Sutdent</h1>
      {props.update ? 
      (props.studentObj && <>
      <div>
        <div className="form-group">
          <label htmlFor="cid">CID</label>
          <input
            type="text"
            className="form-control"
            id="cid"
            name="cid"
            value={props.studentObj.cid}

          />
        </div>
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            className="form-control"
            id="name"
            name="name"
            placeholder={props.studentObj.name}
            onChange={(e)=>{setName(e.target.value)}}
          />
        </div>
        <button onClick={(e)=>handleUpdateName(e)} type="submit" className="btn btn-success" style={{ marginTop: '10px' }}>
          Update
        </button>
      </div> </>) :<div>
        <div className="form-group">
          <label htmlFor="cid">CID</label>
          <input
            type="text"
            className="form-control"
            id="cid"
            name="cid"
            value={formData.cid}
            onChange={handleChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={formData.name}
            onChange={handleChange}
          />
        </div>
        <button onClick={(e)=>handleSubmit(e)} type="submit" className="btn btn-primary form-control" style={{ marginTop: '10px' }}>
          Submit
        </button>

      </div>}
      
    </form>
  );
}

export default Form;