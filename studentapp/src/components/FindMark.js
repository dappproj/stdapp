// import React, { Component } from "react";

// class Find extends Component {
//     render() {
//         return(
//                 <form className="p-5 border " onSubmit={(event) => {
//                     event.preventDefault()
//                     this.props.createMarks(
//                         this.studentid.value,   //studentid display the name of student assigned to that student id
//                         this.subjectid.value,   //subjectid retrieve the value of the particular subject for the particular id
//                         this.grades.value)      //studentid, subjectid and grades are assigned as a value to createMarks 
//                 }}>
               
//                 <h2>Add Marks</h2>

//                 <select id="studentid" className="form-select mb-2" ref={( input) => {this.studentid = input;}}>
//                     <option defaultValue={true}>Open this select menu</option>
//                     {
//                         this.props.students.map((student, key) => {
//                             return (
//                                 <option value={student.cid}>{student.cid}{student.name}</option>
//                             )
//                         })
//                     }
//                 </select>


//                 <select id="subjectid" className="form-select mb-2" ref={( input) => {this.subjectid = input;}}>
//                     <option defaultValue={true}>Open this select menu</option>
//                     {
//                         this.props.subjects.map((subject, key) => {
//                             return (
//                                 <option value={subject.code}>{subject.code}{subject.name}</option>
//                             )
//                         })
//                     }
//                 </select>

//                 <select id="grades" className="form-select mb-2" ref={(input) => {this.grades = input; }}>
//                     <option defaultValue={true}>Open this select menu</option>
//                     <option value="1">A</option>
//                     <option value="2">B</option>
//                     <option value="3">C</option>
//                     <option value="4">D</option>
//                     <option value="5">F</option>

//                 </select>
//                 <input className="form-control btn btn-primary" type="submit" hidden="" />

//                     {/* <div className="mb-3 mt-3">
//                         <input type="text" className="form-control" id="cid" placeholder="CID" required/>
//                     </div>
//                     <div className="mb-3">
//                         <input type="text" className="form-control" id="code" placeholder="Subject code" required/>
//                     </div>
//                     <div className="mb-3">
//                         <input type="text" className="form-control" id="grades" placeholder="Student's grades" required/>
//                     </div>
                    
//                     <input type="submit" className="form-control btn btn-success" hidden=""/> */}
//                 </form>

//         );
//     }
// }

// export default MarkList;

import React, {Component} from 'react';

class FindMarks extends Component{
    //initialise the variables stored int eh state
    constructor(props){
        super(props)
        this.state ={
            marks:"",
        }
    }
    render(){
        return (
          <form className='p-5 border' onSubmit={(event) => {
            event.preventDefault()
            this.props.findMarks(
            this.studentid.value,
            this.subjectid.value)
            .then((result) =>{
                this.setState({marks:result.grades})
            })
          }}>
            <h2>Find Marks</h2>
            <select id='student_id' className='form-select mb-2' style={{ border: '1px solid black', padding: '10px', margin: '10px auto', width: '100%', textAlign: 'center' }}
            ref={(input) => { this.studentid = input;}}>
            <option defaultValue={true}>Open this select menu</option>
            {
              this.props.students.map((student,key) => {
                return(
                  <option value={student.cid}>{student.cid}{student.name}</option>
                )
              })
            }
            </select>
            <select id='subject_id' className='form-select mb-2' style={{ border: '1px solid black', padding: '10px', margin: '10px auto', width: '100%', textAlign: 'center' }}
            ref={(input) => { this.subjectid = input;}}>
            <option defaultValue={true}>Open this select menu</option>
            {
              this.props.subjects.map((subject,key) => {
                return(
                  <option value={subject.code}>{subject.code}{subject.name}</option>
                )
              })
            }
            </select> 
           <input className='form-control btn btn-primary' type="submit" hidden=""></input>
           <p>Grades:{this.state.marks}</p>
          </form>
        )
      }
}

export default FindMarks;