import React, { useState } from 'react';

function SubForm(props) {
  const [name, setName] = useState("")
  const [formData, setFormData] = useState({
    code: '',
    name: '',
  });

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const{code, name}= formData
    // if (formData.code === "" || formData.name === "") {
    //   alert("Code and name cannot be empty");
    //   return;
    // }
    if(code==="" || name ===""){
      alert("code and name cannot be empty")
    }
    props.createSubject(code, name);
    setFormData({
      code: '',
      name: '',
    });
  };

  const handleUpdatesubName = (e) => {
    e.preventDefault();
    props.updatesubName(props.subjectObj._id, name);
    // setFormData({
    //   code: '',
    //   name: '',
    // });
  };

  return (
    <form onSubmit={(e) => handleSubmit(e)}  className="p-5 border justify-content-center w-100">
      <h1>Add Subject</h1>
      {props.update ? 
        props.subjectObj && <>
      <div>
        <div>
          <div className="form-group">
            <label htmlFor="code">CODE</label>
            <input
              type="text"
              className="form-control"
              id="code"
              name="code"
              value={props.subjectObj.code}
            />
          </div>
          <div className="form-group">
            <label htmlFor="name">Subject Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              placeholder={props.subjectObj.name}
              onChange={(e)=>setName(e.target.value)}
            />
          </div>
          <button onClick={(e) => handleUpdatesubName(e)} className="btn btn-success" style={{ marginTop: '10px' }}>
            Update
          </button>
        </div>
      </div></> : <div>
        <div>
          <div className="form-group">
            <label htmlFor="code">CODE</label>
            <input
              type="text"
              className="form-control"
              id="code"
              name="code"
              value={formData.code}
              onChange={handleChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              value={formData.name}
              onChange={handleChange}
            />
          </div>
          <button className="btn btn-primary form-control" style={{ marginTop: '10px' }}>
            Submit
          </button>
        </div>
      </div>}
    </form>
  );
}

export default SubForm;
