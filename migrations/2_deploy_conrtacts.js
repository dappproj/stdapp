var StudentList = artifacts.require("./StudentList.sol");
var SubjectList = artifacts.require("./SubjectList.sol");
var MarktList = artifacts.require("./MarkList.sol");
module.exports= function(deployer){
    deployer.deploy(StudentList);
    deployer.deploy(SubjectList);
    deployer.deploy(MarktList);
}