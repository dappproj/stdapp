// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract MarkList{
    //store marks count
    uint256 public marksCount = 0;
    enum Grades{A, B, C, D, FAIL}
    //model a marks
    struct Marks{
        uint cid;
        string code;
        Grades grades;
    }
    // store marks
    mapping(uint => mapping(string => Marks)) public marks;
    //event
    event addMarksEvent( 
        uint indexed cid, 
        string indexed Code,
        Grades grades
    );

    event updateMarksEvent(
        uint indexed cid,
        string indexed code,
        Grades grades
    );

    // contructor foe students
    constructor() {
        //addMarks( 1001, "CSC101", 1);
    }
    function addMarks(uint _cid, string memory _code, Grades _grade) public{
        // require that marks with cid is not added before
        require(bytes(marks[_cid][_code].code).length == 0, "Marks not found"); // _cid is from studentlist and _code is from subjectlist
        marksCount++;
        marks[_cid][_code] = Marks(_cid, _code, _grade); // define marks
        emit addMarksEvent(_cid, _code, _grade);
    }
    //fetch marks -- simply view the grades of marks
    function findMarks(uint _cid, string memory _code) public view returns(Marks memory){ // return cid, code  -- view - doesnot need to pay the gas fee
        return marks[_cid][_code];

    }

    //update marks
function updateMarks(uint _cid, string memory _code, Grades n_grades) public {

        require(_cid > 0, "cid not found");
        require(bytes(_code).length > 0, "Code not found.");
        marks[_cid][_code].grades = n_grades;

        emit updateMarksEvent(_cid, _code, n_grades);
    }
}