//SPDX-License-Identifier:MIT
pragma solidity ^0.8.0;

contract StudentList{
    uint public studentsCount = 1;
    //model a student
    struct Student {
        uint _id;
        uint cid;
        string name;
        bool graduated;
    }
    //store student
    mapping (uint => Student) public students;
    //constructor for students
    constructor () {
        // createStudent(1001, "Chador Wangdi");
    }
    //events
    event createStudentEvent(
        uint _id,
        uint indexed cid,
        string name,
        bool graduted
    );
    //Event for graduation status
    event markGraduatedEvent(
        uint indexed cid
    );
    //change graduation status of students
    function markGraduated(uint _id) public returns (Student memory){
        students[_id].graduated = true;
        //trigger create event
        emit markGraduatedEvent(_id);
        return students[_id];
    }
    //Fetch student info from storage
    function findStudent(uint _id) public view returns(Student memory){
        return students[_id];
    }
    //create and add student to storage
    function createStudent(uint _studentCid, string memory _name) public returns(Student memory){
        studentsCount++;
        students[studentsCount] = Student(studentsCount, _studentCid, _name, false);
        emit createStudentEvent(studentsCount, _studentCid, _name, false);
        return students[studentsCount];
    }
    event updateStudentEvent( uint id);
    function updateStudent(uint _id, string memory _name) public returns (Student memory) {
        students[_id].name = _name;
        emit updateStudentEvent(_id);
        return students[_id];
    }
}