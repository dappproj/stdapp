//SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;
contract SubjectList {
    uint public subjectsCount = 0;

    struct Subject {
        uint  _id;
        string code;
        string name;
        bool retired;

    }
    mapping(uint => Subject) public subjects;
    //constructor for students
    //constructor(){
        // createSubject(1001,"math");
   // }

    function createSubject(string memory _code, string memory _name)
    public returns (Subject memory) {
        subjectsCount++;
        subjects[subjectsCount] = Subject(subjectsCount, _code, _name, false);
        //tigger create event
        emit createsSubjectEvent(subjectsCount, _code, _name, false);
        return subjects[subjectsCount];
    }

    // Events
    event createsSubjectEvent(
        uint _id,
        string indexed code,
        string name,
        bool retried
    );

    // event for retired status
    event markRetiredEvent(
        uint indexed code
    );

    event updateSubjectEvent(
        uint indexed _id,
        string name
        );

    //change retired status of subject
    function markRetired(uint _id) public returns (Subject memory
    ){
        subjects[_id].retired = true;
        emit markRetiredEvent(_id);
        return subjects[_id];
    }

// fetch student info from storage
    function findSubject(uint _id) public view returns (Subject memory){
        return subjects[_id];
    }

function updateSubject(uint _id,string memory _newname) public returns (Subject memory) {
        // subjects[_id].code = _newcode;
        subjects[_id].name = _newname;
        emit updateSubjectEvent(_id, _newname);
        return subjects[_id];
    }

}